package edu.usu.cs.oo;

public class Driver {
	
	public static void main(String[] args)
	{
		Location location = new Location("100 N", "1821", "Logan", "Utah");
        Company coolCompany = new Company("Cool Company for Attractive People", location);
        Student nate = new Student("Nate Collings", "A00913732", new Job("", 4501, coolCompany));
		System.out.println(nate);
	}

}
