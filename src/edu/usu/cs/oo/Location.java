package edu.usu.cs.oo;

public class Location {

	private String streetAddress;
	private String areaCode;
	private String city;
	private String state;
	
	public Location(String streetAddress, String areaCode, String city, String state) {
		this.streetAddress = streetAddress;
		this.areaCode = areaCode;
		this.city = city;
		this.state = state;
	}

	public String getStreetAddress() {
		return streetAddress;
	}

	public void setStreetAddress(String streetAddress) {
		this.streetAddress = streetAddress;
	}

	public String getAreaCode() {
		return areaCode;
	}

	public void setAreaCode(String areaCode) {
		this.areaCode = areaCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	@Override
	public String toString() {
		return streetAddress + " " + areaCode + " " + city + " " + state;
	}
	
}
