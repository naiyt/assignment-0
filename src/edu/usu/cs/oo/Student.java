package edu.usu.cs.oo;

public class Student {
	
	private String name;
	private String ANumber;
	private Job dreamJob;
	
	public Student(String name, String ANumber, Job dreamJob)
	{
		this.name = name;
		this.ANumber = ANumber;
		this.dreamJob = dreamJob;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getANumber() {
		return ANumber;
	}

	public void setANumber(String aNumber) {
		ANumber = aNumber;
	}

	public Job getDreamJob() {
		return dreamJob;
	}

	public void setDreamJob(Job dreamJob) {
		this.dreamJob = dreamJob;
	}
	
	@Override
	public String toString()
	{
		return "Name: " + name + ", A Number: " + ANumber + ", Job: " + dreamJob.toString();
	}
	
}
